$(document).ready(() => {
  let jq = (() => {
    return {
      modules: {}
    };
  })();

  jq.modules.accordio = (() => {
    /**
     * Initialize event
     */
    function _init() {
      $(".accordion").on("click", e => {
        jq.modules.accordio.toggle(e.target);
      });
    }

    /**
     * Open/Close accordion
     * @param {*} e
     */
    function _toggle(e) {
      $(e).toggleClass("active");
      let panel = $(e).next();

      if ($(panel).css("max-height") != "0px") {
        $(panel).css("max-height", 0);
      } else {
        //Close other accordion
        $(".panel").css("max-height", "0px");
        $(".accordion").removeClass("active");
        //Open clicked accordion
        $(e).addClass("active");
        let height = $(panel).prop("scrollHeight");
        $(panel).css("max-height", height + "px");
      }
    }

    return {
      init: _init,
      toggle: _toggle
    };
  })();

  jq.modules.accordio.init();
});
